#include "builder.hpp"
#include <gtkmm.h>
#include <iostream>
#include <thread>

using namespace std;

class Window : public Gtk::Window {
public:
  Window(BaseObjectType *cobject, Builder &builder) : Gtk::Window(cobject) {
    auto names = builder.get_widget<Gtk::ComboBoxText>("names");
    auto add_name_entry = builder.get_widget<Gtk::Entry>("add_name_entry");
    auto add_name_button = builder.get_widget<Gtk::Button>("add_name_button");
    auto status = builder.get_widget<Gtk::Label>("status");

    names->append("Ola");
    names->append("Kari");

    add_name_button->signal_clicked().connect([add_name_entry, names, status] {
      auto text = add_name_entry->get_text();
      if(text.empty())
        return;

      names->append(text);
      add_name_entry->set_text("");

      status->set_text(text + " was added");
    });

    names->signal_changed().connect([status, names] {
      status->set_text(names->get_active_text() + " was selected");
    });

    show_all();
  }
};

int main() {
  Gtk::Main gtk_main;

  auto builder = Builder::create();
  try {
    builder->add_from_file("view.glade");
  }
  catch(const Glib::Exception &e) {
    cerr << e.what() << endl;
    return 1;
  }
  auto window = builder->get_widget_derived<Window>("window");

  auto css_provider = Gtk::CssProvider::get_default();
  try {
    css_provider->load_from_path("style.css");
  }
  catch(const Glib::Exception &e) {
    cerr << e.what() << endl;
    return 1;
  }
  window.get_style_context()->add_provider_for_screen(Gdk::Screen::get_default(), css_provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  gtk_main.run(window);
}
