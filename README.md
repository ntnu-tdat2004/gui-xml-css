# GUI examples with XML and CSS files

## Prerequisites
* Linux or MacOS
* The C++ IDE [juCi++](https://github.com/cppit/jucipp)

## Installing dependencies

### Debian based distributions
`sudo apt-get install libgtkmm-3.0-dev`

### Arch Linux based distributions
`sudo pacman -S gtkmm3`

### MacOS
`brew install gtkmm3`

## Compile and run
```sh
git clone https://gitlab.com/ntnu-tdat2004/gui-xml-css
juci gui-xml-css
```

Open one of the main cpp files and choose Compile and Run in the Project menu.
