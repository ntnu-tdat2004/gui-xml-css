#include "builder.hpp"
#include <gtkmm.h>
#include <iostream>
#include <thread>

using namespace std;

class Window : public Gtk::Window {
  Gtk::Box vbox;
  Gtk::Box add_name_hbox;

  Gtk::ComboBoxText names;
  Gtk::Entry add_name_entry;
  Gtk::Button add_name_button;
  Gtk::Label status;

public:
  Window() : vbox(Gtk::Orientation::ORIENTATION_VERTICAL), add_name_button("Add name") {
    vbox.add(names);
    vbox.add(add_name_hbox);
    vbox.add(status);
    add(vbox);

    add_name_hbox.add(add_name_entry);
    add_name_hbox.add(add_name_button);

    names.append("Ola");
    names.append("Kari");

    add_name_button.signal_clicked().connect([&] {
      auto text = add_name_entry.get_text();
      if(text.empty())
        return;

      names.append(text);
      add_name_entry.set_text("");

      status.set_text(text + " was added");
    });

    names.signal_changed().connect([&] {
      status.set_text(names.get_active_text() + " was selected");
    });

    show_all();
  }
};

int main() {
  Gtk::Main gtk_main;

  Window window;

  auto css_provider = Gtk::CssProvider::get_default();
  try {
    css_provider->load_from_path("style.css");
  }
  catch(const Glib::Exception &e) {
    cerr << e.what() << endl;
    return 1;
  }
  window.get_style_context()->add_provider_for_screen(Gdk::Screen::get_default(), css_provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  gtk_main.run(window);
}
