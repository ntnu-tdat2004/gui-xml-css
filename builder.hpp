#ifndef BUILDER_HPP_
#define BUILDER_HPP_
#include <gtkmm.h>
#include <memory>
#include <unordered_map>

class Builder : public Gtk::Builder {
public:
  static Glib::RefPtr<Builder> create() {
    return Glib::RefPtr<Builder>(new Builder());
  }

  template <class Widget_T>
  Widget_T *get_widget(const Glib::ustring &name) {
    Widget_T *widget=nullptr;
    Gtk::Builder::get_widget(name, widget);
    return widget;
  }

  template <class Widget_T>
  std::unique_ptr<Widget_T> get_window_widget(const Glib::ustring &name) {
    Widget_T *widget=nullptr;
    Gtk::Builder::get_widget(name, widget);
    return std::unique_ptr<Widget_T>(widget);
  }

  template <class Widget_t>
  Widget_t get_widget_derived(const Glib::ustring &name) {
    auto cwidget = reinterpret_cast<typename Widget_t::BaseObjectType *>(get_cwidget(name));
    return Widget_t(cwidget, *this);
  }
};

#endif /* BUILDER_HPP_ */